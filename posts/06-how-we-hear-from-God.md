---
title: How we hear from God
description: God is speaking, but we need to listen and learn to recognize the voice of God
date: 2022-05-22
tags:
  - Sermon
---

{% callout "info" %}This is a highly abridged version of Pastor Greg's sermon. Watch the full version at the bottom of this page.{% endcallout %}

## Introduction

I don't think that anyone would argue that God is a supernatural being. It's ironic that when we are living the spiritual life, we focus on principles and things of the natural realm, all while dealing with a supernatural being. Websters even defines spiritual as "relating to supernatural being(s)".

When we talk about spiritual life, we are saying that we are relating to a supernatural being. Then many Christians in America flip this and claim that we don't do supernatural things to relate to a supernatural being. We try to keep it all in the natural. God is supernatural, so we need to engage in the supernatural to have a relationship.

In this passage, Jesus is talking to the religious leaders of the day. He says:

> And the Father who sent me has himself borne witness about me. His voice you have never heard, his form you have never seen, and you do not have his word abiding in you, for you do not believe the one whom he has sent. You search the Scriptures because you think that in them you have eternal life; and it is they that bear witness about me, yet you refuse to come to me that you may have life.
>
> - John 5:37–40 (ESV)

Here Jesus is saying the spiritual leaders, "you have all the academics, but you don't know how to relate to a spiritual supernatural being." We know in Exodus 33:11 that Moses heard God's voice. God talks to His leaders, they can't lead if they don't know what God wants, so Jesus is telling the pharisees, "you're supposed to be leading the nation, and you don't even know what God sounds like." This passage tells us that a supernatural conversation is supposed to be a normal thing. Following Christ isn't an academic pursuit. It is a relational pursuit where we are experiencing Jesus!

## Peter and Cornelius

In Acts 10:1-48, Peter has a supernatural encounter. Cornelius was a centurion in the Roman army. Because of this, in Acts 11:1-18, the church leadership calls Peter in so that he can explain his actions. This is the modern day equivalent of a Ukrainian pastor meeting with one of the commanders in the Russian army and leading him and his household to Christ. Not only that, but if the pastor in our story would come back and tell the church of the spiritual encounter he had.

So how'd Peter explain the supernatural activity that led him into the home of Cornelius?

> But Peter began and explained it to them in order: "I was in the city of Joppa praying..."
>
> - Acts 11:4–5a (ESV)

The first thing we see here is that Peter was praying. So many people complain about a lack of God's activity in their life and they're not even talking to God. We need to spend more time in prayer, not to tell God what's going on. He already knows! The main thing we need to do in prayer is ask God for his perspective so that he can help us understand what's going on.

> "... and in a trance I saw a vision, something like a great sheet descending, being let down from heaven by its four corners, and it came down to me. Looking at it closely, I observed animals and beasts of prey and reptiles and birds of the air."
>
> - Acts 11:5b-6 (ESV)

The Greek word used here for the word "trace" means: "although awake, the mind is drawn off surrounding objects and is wholly fixed on forms and images lying within." Have you been so deeply engrossed in thought that you weren't aware of what was going on around you? This phenomenon happens to all of us and is the Biblical definition of a trace. Our eyes are open and seeing, but we're so involved with what's going on inside us that we're not there.

Secondly, Peter saw a vision and the Greek word for this means: "that which is seen." This definition tells us that Peter quite literally saw and experienced this. It's interesting that everyone can give examples of what an evil urge is. However, we struggle to define and recognize a godly urge, like the one Peter was having.

> "And I heard a voice saying to me, 'Rise, Peter; kill and eat.' But I said, 'By no means, Lord; for nothing common or unclean has ever entered my mouth.' But the voice answered a second time from heaven, 'What God has made clean, do not call common.' This happened three times, and all was drawn up again into heaven."
>
> - Acts 11:7–10 (ESV)

So Peter was praying. Then he was in a trace, then had a vision. Then he heard a voice. Peter is explaining to the Church the lengths that God went to, to overcome his own reluctance to go to the house of the enemy.

Does God speak to us? Yes! Does he always talk to us so that we can hear him like would would a physical person? No. In fact, I've never heard the physical voice of God. People have told me that I have good intuition or a sixth-sense, but I know that is really just God speaking to me. God says, "if you know my word, you'll know when I'm talking and it's me."

## Now it's your turn

Picture yourself sitting down across the table with Jesus. Jesus says to you, "Hey, I really need you to start ..." or "I wish you would ...". How does Jesus finish this question. Most of us can easily fill in the rest of this sentence, but we never take the time to slow down, pray, and listen.

https://www.youtube.com/watch?v=yAGWzSVNXTU
