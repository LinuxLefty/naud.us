---
title: Peter and the Holy Spirit
description: Peter had a devine appointment and was led by the Holy Spirit outside his comfort zone.
date: 2022-05-31
tags:
  - Sermon
---

{% callout "info" %}This is a highly abridged version of Pastor Greg's sermon. Watch the full version at the bottom of this page.{% endcallout %}

## Introduction

We have to be careful that we don't fall into the trap that somehow reality is defined only by all my experiences. If we do, it's easy to think that if it's not on my list of experiences, then it's not real. If God is God, I think we should expect some interactions with him to be above and beyond our understanding.

### Recap of last week's sermon

##### 1. Peter was praying

> But Peter began and explained it to them in order: "I was in the city of Joppa praying ...
>
> - Acts 11:4-5a ESV

Supernatural activity generally begins with prayer. If we want to see the supernatural, first we must be taking with God

#### 2. Peter fell into a trace and saw a vision

> ... and in a trance I saw a vision, something like a great sheet descending, being let down from heaven by its four corners, and it came down to me. Looking at it closely, I observed animals and beasts of prey and reptiles and birds of the air. 
>
> - Acts 11:5b-6 ESV

#### 3. Peter heard a voice

> And I heard a voice saying to me, 'Rise, Peter; kill and eat.' But I said, 'By no means, Lord; for nothing common or unclean has ever entered my mouth.' But the voice answered a second time from heaven, 'What God has made clean, do not call common.' This happened three times, and all was drawn up again into heaven. 
>
> - Acts 11:7-10 ESV

## 4. Peter had a divine appointment

> And behold, at that very moment three men arrived at the house in which we were, sent to me from Caesarea. 
>
> - Acts 11:11 ESV

A lot of times we fail to understand how much occurs through their day, that is a divine appointment. We don't understand that we were in the right places at the right time with the right materials, not because of coincidence, but because of God.

## 5. Peter responded to the supernatural with increased obedience

> So he invited them in to be his guests. The next day he rose and went away with them, and some of the brothers from Joppa accompanied him. 
>
> - Acts 10:23 ESV

These were gentiles, and they weren't allowed into his home. However, he could sense that God was at work, so he invited them into his house to be his guests. Peter had watched Jesus minister to the gentiles, but it wasn't until now that he was willing to obey and follow the example of Christ. Just like Peter, sometimes we don't obey the teachings of the Bible until they are backed up by the supernatural. We don't obey Jesus' teachings because they take us out of our comfort zone.

## 6. Peter experienced the confirmation of the Holy Spirit

> And the Spirit told me to go with them, making no distinction. These six brothers also accompanied me, and we entered the man's house. 
>
> - Acts 11:12 ESV

We've all known people who have abused the "trump card" of "God told me so". So let's look at the Greek word for "told", which is "lego", which means to "bid, put forth, ask." Knowing the definition, this meant that Peter felt a literal urge to do something. Many times we feel similar urges, but we don't attribute it to the divine. The Holy Spirit can also give us a negative urge, telling us to avoid or back away from something.

### How do we protect ourselves from deception?

> I have stored up your word in my heart, that I might not sin against you. 
>
> - Psalm 119:11 ESV

Having God's word in you is a corrective mechanism that can confirm urges as from God or show that it cannot be from God because it contradicts his word.

The challenge is that although God's activity is perfect, he sends it through imperfect people. Keeping this in mind keeps us humble as we need His help to discern what urges are from Him. If we make a mistake, it could hurt people and make them walk away from Christ. It also keeps us hungry, as it is His Word that is helps us discern between His and our own urges.

> Delight yourself in the LORD, and he will give you the desires of your heart.
>
> - Psalm 37:4 ESV

Some people have misunderstood this to mean, "If I'm happy in God, he'll give me whatever I want." The correct understanding of this verse is, "Find your joy in God and he changes the desires of your heart; He gives you his desires for your heart."

## 7. Peter learned about supernatural activity in other people

> And he told us how he had seen the angel stand in his house and say, 'Send to Joppa and bring Simon who is called Peter; he will declare to you a message by which you will be saved, you and all your household.' 
>
> - Acts 11:13-14 ESV

Dangerous prayer: "God if your angel shows up to talk to any of my family or friends, feel free to drop them my name. If you're revealing yourself to somebody they aren't sure what to do with that revelation, I'm volunteering. If you send them to me, I'll talk to them."

## 8. Peter saw new dimensions of the Holy Spirit

> As I began to speak, the Holy Spirit fell on them just as on us at the beginning. And I remembered the word of the Lord, how he said, 'John baptized with water, but you will be baptized with the Holy Spirit.' If then God gave the same gift to them as he gave to us when we believed in the Lord Jesus Christ, who was I that I could stand in God's way?" 
>
> - Acts 11:15-17 ESV

1. Peter appealed to scripture ("John baptized with water, but you will be baptized with the Holy Spirit.")
2. There was no need for the laying on of hands or praying ("As I began to speak, the Holy Spirit fell on them")
3. The early church didn't think the gospel and the Holy Spirit were from Gentiles, but God chose to give us the same gifts!

Dangerous prayer: "Don't give me what I'm asking for, God. Give me what you want me to have. I don't even know what I'm asking for, but please give me anything and everything that you intent for your life."

https://www.youtube.com/watch?v=chRRAmzuBpE
