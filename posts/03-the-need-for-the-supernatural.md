---
title: The Need for the Supernatural
description: Many Christians believe that discipline is the key to the spiritual life. However, you don't need more discipline ... you need more of the Holy Spirit
date: 2022-05-18
tags:
  - Sermon
---

{% callout "info" %}This is a highly abridged version of Pastor Greg's sermon. Watch the full version at the bottom of this page.{% endcallout %}

## Introduction

Many Christians now think that simply discipline is the key to the Christian life.

They're shouldering the blame for everything and it all comes down to "I don't have the discipline and so that's why all this is happening in my life. If I was more disciplined this stuff wouldn't be happening." And I'm not saying that we don't have skin in the game. But I also know that I read in the Bible that God used imperfect people and he countered their imperfections a lot of times with a supernatural activity.

I mean, let's look at Peter! I don't think we can all say he got quite fully discipled by Jesus properly. Look at his outburst and his expressions in the garden of Gethsemane. And as Jesus was on trial, and then Jesus is executed, he's off hiding, and then he goes back to fish. I don't think we can all say, "he was a really solid disciple," but most people would say "but Peter just needed more discipline. He needed more focus. He needed more commitment."

Can I tell you what changed Peter's life? Jesus showed up. It wasn't Peter getting more committed. Jesus showed up and talked with Peter. And that supernatural revelation shifted him in a way that discipline alone could not do.

### 1. Knowing scripture isn't enough

> You search the Scriptures because you think that in them you have eternal life; and it is they that bear witness about me, yet you refuse to come to me that you may have life.
>
> -- John 5:39-40

Look at what Jesus said. He's talking to the pharisees. 

Christianity isn't an academic exercise... it is a relationship with Jesus Christ. The more you know, it should help you develop a relationship with Christ. Jesus was pointing out the fact that the scriptures pointed to him. Use the scriptures so that you can get to know Him. You can be at the pinnacle of knowing scripture, and it's possible to still not know Him.

Here's the first thing about understanding the need for the supernatural. We're dealing with a living God. What we have to decide is this: if he's living, is he still doing?

The scripture tells us "faith without works is dead." God does not even violate His own word. If God is who he says he is and he doesn't do works, he's dead! Most people have never thought of that when they take that position, that somehow God's gone silent and he doesn't do these things anymore. You better be careful you because even Jesus said, "produce fruit in keeping with repentance." My God's a living God and he's still producing fruit.

### 2. The supernatural begins with knowing God intimately

> But Jesus answered them, "My Father is working until now, and I am working." 
>
> This was why the Jews were seeking all the more to kill him, because not only was he breaking the Sabbath, but he was even calling God his own Father, making himself equal with God. 
>
> So Jesus said to them, "Truly, truly, I say to you, the Son can do nothing of his own accord, but only what he sees the Father doing. For whatever the Father does, that the Son does likewise. For the Father loves the Son and shows him all that he himself is doing. And greater works than these will he show him, so that you may marvel."
>
> -- John 5:17–20 (ESV)

The challenge is we don't always know what he's doing. So, a lot of times we need to start off with our prayer, "God I'm about to pray for something and I am clueless about what you're doing, so the first thing I need you to do is show me enough as I pray... Help me to know what you're doing, because I don't know, and I want to cooperate with you."

Jesus didn't hear what God wanted. Jesus saw it. He knew God so well, he saw God's activity before it happened, but it was based on relationship.

Jesus says, "use my letter to get to know me, but in the end I want you talking to me. That letter helps you to understand who I am, what I am, what I'm like, what I prefer, and what I don't prefer. It helps you learn how I created you, how I have a plan for you. But please don't just read the letters ... talk to me!" God wants us to get to know him so well, that we just know what he wants because we know him.

> The friendship of the LORD is for those who fear him, and he makes known to them his covenant.
>
> -- Psalm 25:14 (ESV)

David's saying here, "God tells his secrets to people who know him." God says, "when you talk to me, I'll tell you things. I want to talk to you just as much as you want to talk to me"

### 3. Jesus expects his followers to do even greater things than he did

> "Truly, truly, I say to you, whoever believes in me will also do the works that I do; and greater works than these will he do, because I am going to the Father."
>
> -- John 14:12 (ESV)

How many would agree that what Jesus did in action and in deed definitely had a supernatural element assigned to it? He fed the five thousand with the kids lunch. He raised the dead, healed the sick, made the blind see, and casted out demons out of people who were possessed.

If Jesus had to be supernatural in what he did, how am I supposed to do more than he did if I don't buy into the supernatural!? If he needed to be supernatural to do what he did, how am I supposed to do that if I'm going to stay in the natural all the time?

### 4. Jesus commanded his followers to seek the same dimension of the Holy Spirit that he had

> "And behold, I am sending the promise of my Father upon you. But stay in the city until you are clothed with power from on high."
>
> -- Luke 24:49 (ESV)

If he's going to call me not only to do what he did, and then even to do more than he did, it makes sense why Jesus said, "then you need to have what I have"

> "But you will receive power when the Holy Spirit has come upon you, and you will be my witnesses in Jerusalem and in all Judea and Samaria, and to the end of the earth."
>
> -- Acts 1:8 (ESV)

Jesus is saying this I have a dimension of the holy spirit that enabled me to be supernatural in who I am and what I do, and Jesus said, "I just told you that you will do the same things and even greater, but you can't do that if I don't help you out so i'm going to give you the holy spirit because that's who's been helping me." 

If the son of God needed the Holy Spirit to help him do what he did, how much more do you, and I need that?

There's no way you got everything that you needed at salvation.

Because despite accepting Jesus as our Lord and Savior, we still mess up. We're not perfect, we are being perfected, and so we all stay committed to the process.

### 5. Jesus' power didn't come from his deity, but from the Holy Spirit

We all know that Jesus was the Son of God the day he was born.

Isn't it interesting that we don't read about any of the miracles that he did until he turned age 30?

In Luke chapter four, Jesus is about 30. Now, he goes into the wilderness to fast for 40 days and 40 nights. And here's what's interesting; Jesus returned to Galilee in the power of the Spirit.

So he after he went out, he didn't have the power of the Holy Spirit, but he came back, and now he has the power of the Holy Spirit. This does not threaten His deity. He is the son of God, but now he has the power of the Holy Spirit. 

> The Spirit of the Lord is upon me, because he has anointed me to proclaim good news to the poor. He has sent me to proclaim liberty to the captives and recovering of sight to the blind, to set at liberty those who are oppressed, to proclaim the year of the Lord's favor.
>
> -- Luke 4:18–19 (ESV)

Then we go down to Luke chapter four. He's in the synagogue, and he says this: "the Spirit of the Lord is on me" Ah! Jesus for the first time, says the Holy Spirit is now on me. He has for 30 years never said that. He said "I am the son of God. I am the anointed one." He referred to His deity, but now he's communicating the fact that there are subsequent works of the Holy Spirit now in his life.

Let me say it this way, Jesus didn't refer to his deity as the vehicle for the miracles. He referred to the Holy Spirit coming into Him and on Him as the vehicle for miracles.

> "... how God anointed Jesus of Nazareth with the Holy Spirit and with power. He went about doing good and healing all who were oppressed by the devil, for God was with him."
>
> -- Acts 10:38 (ESV)

https://www.youtube.com/watch?v=FlCkbHYwkS8
