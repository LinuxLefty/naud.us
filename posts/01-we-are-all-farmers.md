---
title: We are all farmers
description: You're a farmer, even if you have never touched dirt before
date: 2022-05-16
tags:
  - Life Farming
---

There are many types of farmers in this world. Strawberry farmers grow strawberries. Chicken farmers raise chickens. Even [snake farmers](https://www.youtube.com/watch?v=RLyRQYWEsxw) are even a thing. However, even if you live in the middle of a city and have never set foot on a farm, you're still a farmer. True, you aren't raising strawberries, chickens, or snakes. However, you're still managing, growing, and cultivating your life.

## Welcome to the world, here's your plot of land

As much as we would all like to live in a world where everyone starts life on an equal footing, that isn't the case. Every person starts with a "plot" in life. Some people are born into situations and challenges which leave them with a small plot to work with. Others start off with opportunities and resources most people can only dream about.

<img src="/img/posts/01-plots.excalidraw.png" alt="Not everyone starts with the same sized plot in life" title="But at least everyone has a funny hat">

## Farm Your Life

However, in life, and in farming, it isn't the size of the plot that matters, but rather how the resources are managed. A small well-tended field with rich soil, yields more bountiful harvests than a large area which is neglected and poorly managed.

The trick is that a fruitful life, like a successful farm, requires constant care, vigilant attention, and relentless fortitude. You can't "cram" on a farm, planting your spring vegetables in the fall, working extra hard to make up for lost time. You have to invest in your soil, making sure that it is as rich and as fertile as possible. You have to be intentional about what you are planting in your life, weeding out whatever is taking up resources and not bearing any fruit.

If you're looking to ditch life hacks and easy solutions, and want lasting and organic success, then you've come to the right place. Pull up a chair, [become a Life Farmer](/tags/life-farming), and stay a while.

<img src="/img/posts/01-tomatoes.excalidraw.png" alt="It's not the size of the plot, but the size of the harvest that matters" title="Wait until she sees the truckload of zucchinis">
