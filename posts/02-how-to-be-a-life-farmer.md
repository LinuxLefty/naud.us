---
title: How to be a life farmer
description: Life Farming is all about commiting yourself to maximixing the output from your plot of life.
date: 2022-05-17
tags:
  - Life Farming
---

[Life Farming](/tags/life-farming) is all about committing yourself to maximizing the output from your plot of life. But practically how is that done?

Let's explore this by continuing the farming analogy. Put on your virtual overalls, your straw hat, and let's pretend you're a farmer. What's the first thing that you need to do?

## What do you want to harvest?

The first thing you need to define as a farmer is what outcome you want. Do you want chicken eggs? Strawberries? Pork? Zucchini? Kumquats? Of course, there can be times where you change your mind, and surprise opportunities can drop into your lap. However, until you take the time to figure out what you want to harvest in the future, you have no idea how to start.

As Zig Ziggler put it,

> If you aim at nothing, you hit it every time

Notice that in our farming analogy, we need to define if our goal is "chicken meat" or "chicken eggs". Merely saying "chicken" isn't specific enough. The breeds, the size of your flock, and how you feed them all depend on this decision. Likewise, we said "strawberries" and "zucchini", not "fruits" and "vegetables".

<img src="/img/posts/02-planting.excalidraw.png" alt="Step 1: Decide what you want to harvest" title="He is a simple man. He wants to eat stuff, so he wants to plant stuff">

## Is this a someday or today goal?

We all have limited time and resources, just as farmers have limited space in a field to raise crops. Because you can't fit everything in, you need to determine what you're willing to plant now in your life's field. The rest of those goals are still awesome, but save them for a future season.

<img src="/img/posts/02-someday.excalidraw.png" alt="Step 2: Define your current goals" title="This is totally not based on a conversation my wife had with me">

## What's the next step?

Lastly, you need to decide on a plan. The plan can be rather vague, but you need to at least know the next couple of actions. If your goal is chicken eggs, you need a chicken coop first. If your goal is to harvest zucchini, you need some basic gardening implements and prepared soil before you can begin.

<img src="/img/posts/02-next.excalidraw.png" alt="Step 3: Next actions" title="Just wait until she sees the piglets in the kitchen sink">

## Putting it all together

Dust yourself off and hang up your overalls for now. Let's review the steps we learned:

1. Define what you want to harvest
2. Determine your current goals
3. Decide on next actions

Next, we're going through each one of these steps in detail. Check out the [full list of articles in this series](/tags/life-farming).
