---
layout: layouts/post.njk
title: About Me
templateClass: tmpl-post
eleventyNavigation:
  key: About Me
  order: 3
---

<article class="media">
  <figure class="media-left">
    <p class="image is-128x128">
      <img class="is-rounded" src="/img/farmer-pete.jpg">
    </p>
  </figure>
  <div class="media-content">
    <div class="content">
      <p>Hi! My name is <b>Peter Naudus</b>. People might also know me as <b>"Farmer Pete"</b>.</p>
      <p>If you want to get in contact with me, email me at <code>peter&commat;naud&period;us</code></p>
    </div>
  </div>
</article>
